function ajax(){
    
    let id_buscar = document.getElementById('id').value

    if (id_buscar > 10 || isNaN(id_buscar)){

        window.alert("El ID que intentas buscar no existe!!");


    }
    
    const url = "https://jsonplaceholder.typicode.com/users";
    axios.get(url,{
        paramers: {
            id: id_buscar
        }
    })
    .then((res)=>{
        console.log(res)
        mostrar(res.data)
    }).catch((err)=>{
        console.log("Surgio un error " + err)
    })

    function mostrar(data){

        for(let item of data){
            if(item.id == id_buscar){
                document.getElementById('nombre').value = item.name;
                document.getElementById('nombreUsuario').value = item.username;
                document.getElementById('email').value = item.email;
                document.getElementById('calle').value = item.address.street;
                document.getElementById('numero').value = item.address.suite;
                document.getElementById('ciudad').value = item.address.city;
                
            }
            
        }// for

    }// mostrar
    
}// ajax

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click',function(){
    ajax();
})

    document.getElementById('btnLimpiar').addEventListener('click', function(){
    document.getElementById('nombre').value = "";
    document.getElementById('nombreUsuario').value = "";
    document.getElementById('email').value = "";
    document.getElementById('calle').value = "";
    document.getElementById('numero').value = "";
    document.getElementById('ciudad').value = "";
});